class Keyboard {

    // Allows the user to specify keycodes that are evaluated as pressed or not pressed
    // The user is able to get the status of a particular key

    keys: Object;

    constructor() {
        this.keys = {};
        this._setup_default_keys();
    }

    _setup_default_keys() {
        this.keys['left'] = new CustomKey(37);
        this.keys['up'] = new CustomKey(38);
        this.keys['right'] = new CustomKey(39);
        this.keys['down'] = new CustomKey(40);
        this.keys['space'] = new CustomKey(32);
        this.keys['control'] = new CustomKey(17);
    }

    addKey(keycode) {
        this.keys[keycode] = new CustomKey(keycode);
    }

    isKeyPressed(keycode) {
        return this.keys[keycode].isDown;
    }
}

class CustomKey {

    // Represents a single key
    // Registers the given keycode, and tracks if it is pressed or not

    code: number;
    isDown: boolean;
    isUp: boolean;
    press: any;
    release: any;
    downHandler: Function;
    upHandler: Function;
    key: Key;

    constructor(keycode) {
        var that = this;
        this.code = keycode;
        this.isDown = false;
        this.isUp = true;

        this.press = undefined;
        this.release = undefined;

        this.downHandler = function(event) {

            if (event.keyCode === that.code) {

                if (that.isUp) {
                    that.isDown = true;
                    that.isUp = false;
                }
            }
            event.preventDefault();
        };

        this.upHandler = function (event) {
            if (event.keyCode === that.code) {
                that.isDown = false;
                that.isUp = true;
            }
            event.preventDefault();
        };

        //Attach event listeners
        window.addEventListener(
            "keydown", this.downHandler.bind(this.key), false
        );
        window.addEventListener(
            "keyup", this.upHandler.bind(this.key), false
        );
    }
}

