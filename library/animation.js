/// <reference path="../external_libraries/pixi.js.d.ts" />
var Animation = (function () {
    function Animation(texture_sheet, rect, nbr_frames, loop_time, xpadding) {
        if (nbr_frames === void 0) { nbr_frames = 1; }
        if (loop_time === void 0) { loop_time = 0.1; }
        if (xpadding === void 0) { xpadding = 0; }
        this.frames = [];
        for (var i = 0; i < nbr_frames; i++) {
            var source_rect = new PIXI.Rectangle(rect.x + i * rect.width + xpadding * i, rect.y, rect.width, rect.height);
            this.frames.push(new PIXI.Texture(texture_sheet.baseTexture, source_rect));
        }
        this.current_frame = 0;
        this.delta_anim_time = loop_time / this.frames.length;
        this.next_anim_time = 0;
        this.new_frame_ready = false;
    }
    Animation.prototype.update = function (game_time) {
        if (this.next_anim_time === 0) {
            this.next_anim_time = game_time.get_time() + this.delta_anim_time;
        }
        else if (game_time.get_time() >= this.next_anim_time) {
            this.current_frame++;
            //console.log(this.frames.length);
            if (this.current_frame >= this.frames.length) {
                this.current_frame = 0;
            }
            this.next_anim_time = game_time.get_time() + this.delta_anim_time;
            this.new_frame_ready = true;
        }
    };
    Animation.prototype.has_new_frame = function () {
        return this.new_frame_ready;
    };
    Animation.prototype.get_current_frame = function () {
        this.new_frame_ready = false;
        return this.frames[this.current_frame];
    };
    return Animation;
})();
