/// <reference path="game_objects.ts" />
/// <reference path="keyboard.ts" />
/// <reference path="collision_handling.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Direction = {
    'LEFT': 1,
    'UP': 2,
    'RIGHT': 3,
    'DOWN': 4,
    'NONE': 0
};
var EdgeBehaviour = {
    'NONE': 1,
    'STOP': 2,
    'STOP_PLATFORM': 3,
    'BOUNCE': 4
};
var Movement = (function () {
    function Movement(movement_object, gc) {
        this.LEVEL_WIDTH = gc.level_width;
        this.LEVEL_HEIGHT = gc.playable_area_height;
        this.LEVEL_HEIGHT_OFFSET = gc.playable_area_offset;
        this.movement_object = movement_object;
        this.xspeed = 0;
        this.yspeed = 0;
        this.edge_behaviour = EdgeBehaviour.NONE;
        this.is_disabled = false;
    }
    Movement.prototype.get_xspeed = function () {
        return this.xspeed;
    };
    Movement.prototype.set_xspeed = function (val) {
        this.xspeed = val;
    };
    Movement.prototype.get_yspeed = function () {
        return this.yspeed;
    };
    Movement.prototype.set_yspeed = function (val) {
        this.yspeed = val;
    };
    Movement.prototype.get_is_moving = function () {
        return this.xspeed !== 0 || this.yspeed !== 0;
    };
    Movement.prototype.update = function (game_time, obstacles, player) {
        if (this.is_disabled) {
            this.xspeed = 0;
            this.yspeed = 0;
            return;
        }
        this.movement_object.set_x(this.movement_object.get_x() + this.xspeed);
        if (!this.grounded) {
            this.movement_object.set_y(this.movement_object.get_y() + this.yspeed);
        }
        if (this.edge_behaviour !== EdgeBehaviour.NONE) {
            this._handle_edge_behaviour();
        }
    };
    Movement.prototype._handle_edge_behaviour = function () {
        var edge_collisions = this.get_edge_collisions();
        if (edge_collisions.length > 0) {
            if (this.edge_behaviour == EdgeBehaviour.BOUNCE) {
                this._bounce_at_edges(edge_collisions);
            }
            else if (this.edge_behaviour == EdgeBehaviour.STOP || this.edge_behaviour == EdgeBehaviour.STOP_PLATFORM) {
                this._stop_at_edges(this.edge_behaviour, edge_collisions);
            }
            else {
                throw new Error("Unexpected edge behaviour: " + this.edge_behaviour);
            }
        }
    };
    Movement.prototype._bounce_at_edges = function (edges) {
        for (var i = 0; i < edges.length; i++) {
            switch (edges[i]) {
                case Direction.LEFT: {
                    this.xspeed = Math.abs(this.xspeed);
                    break;
                }
                case Direction.UP: {
                    this.yspeed = Math.abs(this.yspeed);
                    break;
                }
                case Direction.RIGHT: {
                    this.xspeed = -Math.abs(this.xspeed);
                    break;
                }
                case Direction.DOWN: {
                    this.yspeed = -Math.abs(this.yspeed);
                    break;
                }
                default: {
                    break;
                }
            }
        }
    };
    Movement.prototype._stop_at_edges = function (behaviour, edges) {
        for (var i = 0; i < edges.length; i++) {
            switch (edges[i]) {
                case Direction.LEFT: {
                    this.movement_object.set_x(0);
                    break;
                }
                case Direction.UP: {
                    this.movement_object.set_y(this.LEVEL_HEIGHT_OFFSET);
                    this.yspeed = 0;
                    break;
                }
                case Direction.RIGHT: {
                    this.movement_object.set_x(this.LEVEL_WIDTH - this.movement_object.width());
                    break;
                }
                case Direction.DOWN: {
                    this.movement_object.set_y(this.LEVEL_HEIGHT + this.LEVEL_HEIGHT_OFFSET - this.movement_object.height());
                    if (behaviour === EdgeBehaviour.STOP_PLATFORM) {
                        this.is_jumping = false;
                        this.grounded = true;
                    }
                    this.yspeed = 0;
                    break;
                }
                default: {
                    break;
                }
            }
        }
    };
    Movement.prototype.get_edge_collisions = function () {
        var edge_collisions = [];
        if (this.movement_object.get_x() < 0) {
            edge_collisions.push(Direction.LEFT);
        }
        else if (this.movement_object.get_x() + this.movement_object.width() > this.LEVEL_WIDTH) {
            edge_collisions.push(Direction.RIGHT);
        }
        if (this.movement_object.get_y() < this.LEVEL_HEIGHT_OFFSET) {
            edge_collisions.push(Direction.UP);
        }
        if (this.movement_object.get_y() + this.movement_object.height() > this.LEVEL_HEIGHT + this.LEVEL_HEIGHT_OFFSET) {
            edge_collisions.push(Direction.DOWN);
        }
        return edge_collisions;
    };
    return Movement;
})();
var PlayerHorizontalScroll = (function (_super) {
    __extends(PlayerHorizontalScroll, _super);
    function PlayerHorizontalScroll(game_object, gc) {
        _super.call(this, game_object, gc);
        this.keyboard = new Keyboard();
        this.RISINGSPEED = 0.24; /*0.75;*/
        this.MAX_SPEED = 4;
        this.MIN_SPEED_THRESHOLD = 0.25;
        //this.GRAVITY = 0.1;
        this.edge_behaviour = EdgeBehaviour.STOP;
        this.next_direction = false;
        this.next_direction_switched = true;
        this.game_content = gc;
    }
    PlayerHorizontalScroll.prototype.update = function (game_time, obstacles, player) {
        _super.prototype.update.call(this, game_time, obstacles, player);
        if (player.height() > this.game_content.playable_area_height) {
            this.is_disabled = true;
            player.set_is_clogged();
        }
        player.set_next_direction(this.next_direction);
        this._update_keyboard_controls();
    };
    PlayerHorizontalScroll.prototype._update_keyboard_controls = function () {
        if (!this.keyboard.isKeyPressed('space') && !this.next_direction_switched) {
            this.next_direction = !this.next_direction;
            this.next_direction_switched = true;
        }
        if (this.keyboard.isKeyPressed('space')) {
            this.next_direction_switched = false;
            if (this.next_direction) {
                this.yspeed -= this.RISINGSPEED;
                this.yspeed = Math.max(this.yspeed, -this.MAX_SPEED);
            }
            else {
                this.yspeed += this.RISINGSPEED;
                this.yspeed = Math.min(this.yspeed, this.MAX_SPEED);
            }
        }
        else if (this.yspeed < 0) {
            if (this.yspeed <= -this.MIN_SPEED_THRESHOLD) {
                this.yspeed += this.RISINGSPEED;
            }
            else {
                this.yspeed = 0;
            }
        }
        else if (this.yspeed > 0) {
            if (this.yspeed >= this.MIN_SPEED_THRESHOLD) {
                this.yspeed -= this.RISINGSPEED;
            }
            else {
                this.yspeed = 0;
            }
        }
    };
    return PlayerHorizontalScroll;
})(Movement);
var SimpleHorizontal = (function (_super) {
    __extends(SimpleHorizontal, _super);
    function SimpleHorizontal(game_object, gc, speed) {
        _super.call(this, game_object, gc);
        this.xspeed = -speed;
    }
    return SimpleHorizontal;
})(Movement);
var AimingHorizontal = (function (_super) {
    __extends(AimingHorizontal, _super);
    function AimingHorizontal(game_object, gc, xspeed, yspeed) {
        _super.call(this, game_object, gc);
        this.xspeed = -xspeed;
        this.MY_YSPEED = yspeed;
    }
    AimingHorizontal.prototype.update = function (game_time, obstacles, player) {
        _super.prototype.update.call(this, game_time, obstacles, player);
        if (player.get_y() < this.movement_object.get_y()) {
            this.yspeed -= this.MY_YSPEED;
        }
        else if (player.get_y() > this.movement_object.get_y()) {
            this.yspeed += this.MY_YSPEED;
        }
    };
    return AimingHorizontal;
})(Movement);
var PlatformMovement = (function (_super) {
    __extends(PlatformMovement, _super);
    function PlatformMovement(game_object, gc) {
        _super.call(this, game_object, game_content);
        this.GRAVITY = 0.3;
        this.FRICTION = 0.8;
        this.MAX_SPEED = 1.5;
        this.JUMPSTRENGTH = 0;
        this.edge_behaviour = EdgeBehaviour.STOP_PLATFORM;
        this.is_jumping = false;
        this.grounded = false;
    }
    PlatformMovement.prototype.update = function (game_time, obstacles, player) {
        _super.prototype.update.call(this, game_time, obstacles, player);
        this.xspeed *= this.FRICTION;
        this.yspeed += this.GRAVITY;
        this._update_obstacle_collision(obstacles);
    };
    PlatformMovement.prototype._update_obstacle_collision = function (obstacles) {
        this.grounded = false;
        for (var i = 0; i < obstacles.length; i++) {
            var obj = obstacles[i];
            var dir = correctingColCheck(this.movement_object, obj);
            if (dir === Direction.LEFT || dir === Direction.RIGHT || dir === Direction.UP || dir === Direction.DOWN) {
            }
            if (dir === Direction.LEFT || dir === Direction.RIGHT) {
                this.is_jumping = false;
            }
            else if (dir === Direction.DOWN) {
                this.is_jumping = false;
                this.grounded = true;
                this.yspeed = 0;
            }
            else if (dir === Direction.UP) {
                this.yspeed = 0;
            }
        }
    };
    return PlatformMovement;
})(Movement);
