/// <reference path="../external_libraries/pixi.js.d.ts" />
/// <reference path="movement.ts" />
/// <reference path="animation.ts" />
/// <reference path="game_utilities.ts" />
/// <reference path="enemies.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
// ------ Classes ------ //
var GameObject = (function () {
    function GameObject(texture_sheet, static_rect, xpos, ypos, gc) {
        this.source_rect = static_rect;
        var base_text = new PIXI.Texture(texture_sheet.baseTexture, new PIXI.Rectangle(static_rect.x, static_rect.y, static_rect.width, static_rect.height));
        this.sprite = new PIXI.Sprite(base_text);
        this.animations = {};
        this.current_anim_name = 'static';
        this.animations[this.current_anim_name] = new Animation(texture_sheet, static_rect);
        this.current_anim = this.animations[this.current_anim_name];
        this.set_x(xpos);
        this.set_y(ypos);
        this.alpha = 1;
        this.movement = undefined;
        this.logging = false;
        this.label = 'gameobject';
        this.scaling = 1;
        this.gc = gc;
        this.next_direction = false;
    }
    GameObject.prototype.set_next_direction = function (val) {
        this.next_direction = val;
    };
    GameObject.prototype.get_next_direction = function () {
        return this.next_direction;
    };
    GameObject.prototype.is_dead = function () {
        return this.current_health <= 0;
    };
    GameObject.prototype.get_sprite = function () {
        return this.sprite;
    };
    GameObject.prototype.change_animation = function (name) {
        if (name === this.current_anim_name) {
            return;
        }
        this.current_anim_name = name;
        this.current_anim = this.animations[this.current_anim_name];
    };
    GameObject.prototype.get_x = function () {
        return this.sprite.position.x;
    };
    GameObject.prototype.set_x = function (value) {
        this.sprite.position.x = value;
    };
    GameObject.prototype.increment_x = function (increment) {
        this.sprite.position.x += increment;
    };
    GameObject.prototype.get_y = function () {
        return this.sprite.position.y;
    };
    GameObject.prototype.set_y = function (value) {
        this.sprite.position.y = value;
    };
    GameObject.prototype.increment_y = function (increment) {
        this.sprite.position.y += increment;
    };
    GameObject.prototype.set_health = function (val) {
        this.current_health = val;
    };
    GameObject.prototype.get_health = function () {
        return this.current_health;
    };
    GameObject.prototype.width = function () {
        return this.source_rect.width * this.scaling;
    };
    GameObject.prototype.height = function () {
        return this.source_rect.height * this.scaling;
    };
    GameObject.prototype.get_alpha = function () {
        return this.sprite.alpha;
    };
    GameObject.prototype.set_alpha = function (value) {
        this.sprite.alpha = value;
    };
    GameObject.prototype.set_scaling = function (val) {
        this.scaling = val;
        this.sprite.scale.x = this.scaling;
        this.sprite.scale.y = this.scaling;
    };
    GameObject.prototype.update = function (game_time, obstacles, game_objects, player) {
        if (this.movement !== undefined) {
            this.movement.update(game_time, obstacles, player);
        }
        if (!this.static_only) {
            this.current_anim.update(game_time);
            if (this.current_anim.has_new_frame()) {
                this.sprite.texture = this.current_anim.get_current_frame();
            }
        }
    };
    GameObject.prototype.get_closest_object = function (objects) {
        var closest_obj = null;
        var closest_dist = 1000;
        for (var i = 0; i < objects.length; i++) {
            var obj = objects[i];
            var distance = this.get_dist_to_object(obj);
            if (distance < closest_dist) {
                closest_dist = distance;
                closest_obj = obj;
            }
        }
        return closest_obj;
    };
    GameObject.prototype.get_dist_to_object = function (other_obj) {
        return Math.sqrt(Math.pow(this.get_x() - other_obj.get_x(), 2) + Math.pow(this.get_y() - other_obj.get_y(), 2));
    };
    GameObject.prototype.collide = function (other_obj) {
    };
    return GameObject;
})();
var PlatformGameObject = (function (_super) {
    __extends(PlatformGameObject, _super);
    function PlatformGameObject(spritesheet, rect, x, y, gc) {
        _super.call(this, spritesheet, rect, x, y, gc);
        this.label = 'platformgameobject';
    }
    PlatformGameObject.prototype.get_yspeed = function () {
        return this.movement.yspeed;
    };
    PlatformGameObject.prototype.set_yspeed = function (value) {
        this.movement.yspeed = value;
    };
    PlatformGameObject.prototype.get_is_jumping = function () {
        return this.movement.is_jumping;
    };
    PlatformGameObject.prototype.set_is_jumping = function (value) {
        this.movement.is_jumping = value;
    };
    PlatformGameObject.prototype.get_grounded = function () {
        return this.movement.grounded;
    };
    PlatformGameObject.prototype.set_grounded = function (value) {
        this.movement.grounded = value;
    };
    return PlatformGameObject;
})(GameObject);
var Player = (function (_super) {
    __extends(Player, _super);
    //get_is_moving() { return this.movement; }
    function Player(x, y, gc, heart_time) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(159, 1, 200, 200), x, y, gc);
        this.movement = new PlayerHorizontalScroll(this, gc);
        this.label = 'player';
        this.game_content = gc;
        this.BULLET_DELAY = 5;
        this.next_shot_time = -1;
        this.CLOG_REDUCTION = 0.5;
        this.INITIAL_SCALE = 0.16;
        this.animations['animated_static'] = new Animation(gc.spritesheet, new PIXI.Rectangle(159, 1, 200, 200), 3, 1, 5);
        this.change_animation('animated_static');
        this.max_health = 80;
        this.current_health = this.max_health;
        this.is_clogged = false;
        this.level_win = false;
        this.clog_amount = 0;
        this.heart_appear_time = heart_time;
    }
    Player.prototype.set_is_clogged = function () {
        this.is_clogged = true;
    };
    Player.prototype.get_is_clogged = function () {
        return this.is_clogged;
    };
    Player.prototype.update = function (game_time, obstacles, game_objects) {
        _super.prototype.update.call(this, game_time, obstacles, game_objects, this);
        if (this.clog_amount < -0.75) {
            this.current_health = 0;
        }
        if (this.is_clogged || this.is_dead()) {
            this.get_sprite().tint = 0xFFFFFF;
            return;
        }
        if (this.is_shot_ready(game_time)) {
            this.get_sprite().tint = 0x00AA00;
        }
        else {
            this.get_sprite().tint = 0xFFFFFF;
        }
        this.set_scaling(this.calculate_scale(this.clog_amount));
    };
    Player.prototype.is_shot_ready = function (game_time) {
        if (this.next_shot_time === -1) {
            this.next_shot_time = game_time.get_time() + this.BULLET_DELAY;
            return false;
        }
        else {
            return game_time.get_time() >= this.next_shot_time;
        }
    };
    Player.prototype.shooting_is_performed = function (game_time) {
        this.next_shot_time = game_time.get_time() + this.BULLET_DELAY;
        if (game_time.get_time_since_start() < this.heart_appear_time) {
            this.clog_amount = Math.max(this.clog_amount - this.clog_amount * this.CLOG_REDUCTION, 0);
        }
    };
    Player.prototype.collide = function (other_obj) {
        if (other_obj instanceof Enemy) {
            this.clog_amount += other_obj.get_damage();
            SOFT_HIT_SOUND2.play();
        }
    };
    Player.prototype.calculate_scale = function (clog_amount) {
        return (clog_amount + 1) * this.INITIAL_SCALE;
    };
    return Player;
})(PlatformGameObject);
var Bullet = (function (_super) {
    __extends(Bullet, _super);
    function Bullet(x, y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(1, 369, 10, 10), x, y, gc);
        var max_speed = 8;
        var min_speed = 2;
        var speed = Math.random() * (max_speed - min_speed) + min_speed;
        this.movement = new SimpleHorizontal(this, gc, -speed);
        this.x_change_speed = 1.2;
    }
    Bullet.prototype.get_x_change_speed = function () {
        return this.x_change_speed;
    };
    Bullet.prototype.update = function (gameTime, obstacles, game_objects, player) {
        _super.prototype.update.call(this, gameTime, obstacles, game_objects, player);
        var rough_screen_width = 1000;
        if (this.get_x() > rough_screen_width) {
            this.set_health(0);
        }
    };
    Bullet.prototype.collide = function (other_obj) {
        if (other_obj instanceof Enemy) {
            this.set_health(0);
        }
    };
    return Bullet;
})(GameObject);
var BloodParticle = (function (_super) {
    __extends(BloodParticle, _super);
    function BloodParticle(x, y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(1, 353, 3, 3), x, y, gc);
        this.TRAIL_SPAWN_DELAY = 0.01;
        this.SPAWN_Y_OFFSET = 50;
        this.trail_spawn_time = 0;
        var max_speed = 6;
        var min_speed = 3;
        var speed = Math.random() * (max_speed - min_speed) + min_speed;
        this.movement = new SimpleHorizontal(this, gc, speed);
    }
    return BloodParticle;
})(GameObject);
var BloodParticleTrail = (function (_super) {
    __extends(BloodParticleTrail, _super);
    function BloodParticleTrail(x, y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(1, 361, 18, 3), x, y, gc);
        this.LIFE_TIME = 0.6;
        this.ALPHA_DECAY_RATE = 0.06;
    }
    return BloodParticleTrail;
})(GameObject);
var DisplayArrow = (function (_super) {
    __extends(DisplayArrow, _super);
    function DisplayArrow(x, y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(1, 295, 20, 17), x, y, gc);
        this.current_direction = false;
        this.Y_OFFSET = 25;
        this.animations['up'] = new Animation(gc.spritesheet, new PIXI.Rectangle(1, 295, 20, 17));
        this.animations['down'] = new Animation(gc.spritesheet, new PIXI.Rectangle(26, 295, 20, 17));
        this.animations['up_inactive'] = new Animation(gc.spritesheet, new PIXI.Rectangle(51, 295, 20, 17));
        this.animations['down_inactive'] = new Animation(gc.spritesheet, new PIXI.Rectangle(76, 295, 20, 17));
    }
    DisplayArrow.prototype.update_arrow = function (player) {
        var is_moving = player.movement.get_is_moving();
        var dir = player.get_next_direction();
        this.set_x(player.get_x() + player.width() / 2 - 8);
        if (dir) {
            if (is_moving) {
                this.change_animation('up');
            }
            else {
                this.change_animation('up_inactive');
            }
            this.set_y(player.get_y() - this.Y_OFFSET);
        }
        else {
            if (is_moving) {
                this.change_animation('down');
            }
            else {
                this.change_animation('down_inactive');
            }
            this.set_y(player.get_y() + player.height() + this.Y_OFFSET - this.sprite.height);
        }
    };
    return DisplayArrow;
})(GameObject);
