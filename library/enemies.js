/// <reference path="game_objects.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Enemy = (function (_super) {
    __extends(Enemy, _super);
    function Enemy(spritesheet, rect, lv_w, y, gc) {
        _super.call(this, spritesheet, rect, lv_w, y, gc);
        this.damage = 0;
    }
    Enemy.prototype.get_damage = function () {
        return this.damage;
    };
    Enemy.prototype.update = function (gameTime, obstacles, game_objects, player) {
        _super.prototype.update.call(this, gameTime, obstacles, game_objects, player);
        if (this.get_x() < -100) {
            this.set_health(0);
        }
    };
    Enemy.prototype.collide = function (other_obj) {
        if (other_obj instanceof Player) {
            this.set_health(0);
        }
        else if (other_obj instanceof Bullet) {
            this.movement.set_xspeed(this.movement.get_xspeed() + other_obj.get_x_change_speed());
            if (this.movement.get_xspeed() > 0) {
                this.set_health(0);
            }
            SOFT_HIT_SOUND.play();
        }
    };
    return Enemy;
})(GameObject);
var SimpleEnemy = (function (_super) {
    __extends(SimpleEnemy, _super);
    function SimpleEnemy(y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(38, 61, 39, 27), gc.level_width, y, gc);
        this.label = 'simpleenemy';
        //var max_speed = 4;
        var min_speed = 8;
        var variance = 2;
        var speed = Math.random() * variance + min_speed; /*Math.random() + (max_speed - min_speed) + min_speed;*/
        this.damage = 0.3;
        this.movement = new SimpleHorizontal(this, gc, speed);
    }
    return SimpleEnemy;
})(Enemy);
var WhiteBloodCell = (function (_super) {
    __extends(WhiteBloodCell, _super);
    function WhiteBloodCell(y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(38, 316, 39, 28), gc.level_width, y, gc);
        this.label = 'whitebloodcell';
        var min_speed = 13;
        var variance = 1;
        var speed = Math.random() * variance + min_speed;
        this.damage = -0.45;
        this.movement = new SimpleHorizontal(this, gc, speed);
    }
    return WhiteBloodCell;
})(Enemy);
var DirectingEnemy = (function (_super) {
    __extends(DirectingEnemy, _super);
    function DirectingEnemy(y, gc) {
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(1, 38, 64, 18), gc.level_width, y, gc);
        this.label = 'directingenemy';
        //var max_speed = 8;
        var min_speed = 8;
        var variance = 2;
        var speed = Math.random() * variance + min_speed; /*Math.random() + (max_speed - min_speed) + min_speed;*/
        var yspeed = 0.05;
        this.damage = 0.6;
        this.movement = new AimingHorizontal(this, gc, speed, yspeed);
    }
    return DirectingEnemy;
})(Enemy);
var HeartEnemy = (function (_super) {
    __extends(HeartEnemy, _super);
    function HeartEnemy(gc) {
        var xpos = 600;
        var ypos = gc.level_height / 2 - 110;
        _super.call(this, gc.spritesheet, new PIXI.Rectangle(206, 234, 160, 192), xpos, ypos, gc);
        this.label = 'heartenemy';
        this.animations['animated_static'] = new Animation(gc.spritesheet, new PIXI.Rectangle(206, 234, 160, 192), 3, 0.4, 5);
        this.change_animation('animated_static');
    }
    return HeartEnemy;
})(GameObject);
