/// <reference path="game_utilities.ts" />
/// <reference path="game_objects.ts" />
/// <reference path="enemies.ts" />
/// <reference path="levels.ts" />

class StateManager {

    stages: Object;
    current_stage: State;
    state_change_time: number;

    constructor(pixi_stage:PIXI.Container, gc:GameContent) {

        this.state_change_time = new Date().getTime() / 1000;

        this.stages = {};
        this.stages['menu'] = new MenuState(this, pixi_stage, gc);
        this.stages['level'] = new LevelInstance(this, pixi_stage, gc);
        this.stages['win'] = new WinState(this, pixi_stage, gc);

        this.current_stage = this.stages['menu'];
        this.current_stage.initialize();
    }

    change_stage(stage_key) {

        this.state_change_time = new Date().getTime() / 1000;

        this.current_stage.deinitialize();
        this.current_stage = this.stages[stage_key];
        this.current_stage.initialize();
    }

    time_reset() {
        this.state_change_time = new Date().getTime() / 1000;
    }

    update() {

        var game_time = new GameTime(this.state_change_time);
        //game_time.update();

        this.current_stage.update(game_time);
    }
}

class State {

    pixi_stage:PIXI.Container;
    background_layer:PIXI.Container = new PIXI.Container();
    game_object_layer:PIXI.Container = new PIXI.Container();
    foreground_layer:PIXI.Container = new PIXI.Container();

    state_manager: StateManager;
    gc: GameContent;

    game_over_text: PIXI.Text;
    background_image: PIXI.Sprite;

    game_objects: GameObject[];
    obstacle_objects: GameObject[];
    background_objects: GameObject[];
    overlay_objects: PIXI.Sprite[];

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent){

        this.state_manager = state_manager;
        this.pixi_stage = pixi_stage;
        this.gc = game_content;

        this.pixi_stage.addChild(this.background_layer);
        this.pixi_stage.addChild(this.game_object_layer);
        this.pixi_stage.addChild(this.foreground_layer);

        this.game_over_text = undefined;
        this.background_image = new PIXI.Sprite(game_content.background);
    }

    initialize() {

        this.state_manager.time_reset();

        this.game_objects = [];
        this.obstacle_objects = [];
        this.background_objects = [];
        this.overlay_objects = [];

        if (this.background_image !== undefined) {
            this.background_layer.addChild(this.background_image);
        }
    }

    deinitialize() {

        // Game objects
        for (var i = 0; i < this.game_objects.length; i++) {
            this.game_object_layer.removeChild(this.game_objects[i].get_sprite());
        }
        this.game_objects = [];

        // Obstacles
        for (var i = 0; i < this.obstacle_objects.length; i++) {
            this.game_object_layer.removeChild(this.obstacle_objects[i].get_sprite());
        }
        this.obstacle_objects = [];

        // Background objects
        for (var i = 0; i < this.background_objects.length; i++) {
            this.background_layer.removeChild(this.background_objects[i].get_sprite());
        }
        this.background_objects = [];

        // Overlay objects
        for (var i = 0; i < this.overlay_objects.length; i++) {
            this.foreground_layer.removeChild(this.overlay_objects[i]);
        }
        this.overlay_objects = [];

        if (this.background_image !== undefined) {
            this.pixi_stage.removeChild(this.background_image);
        }

        if (this.game_over_text !== undefined) {
            this.pixi_stage.removeChild(this.game_over_text);
        }
    }

    update(game_time) {

        //console.log(`${this.game_objects.length} ${this.obstacle_objects.length} ${this.background_objects.length}`);

    }

    add_game_object(obj) {
        this.game_objects.push(obj);
        this.game_object_layer.addChild(obj.get_sprite());
    }

    add_obstacle_object(obj) {
        this.obstacle_objects.push(obj);
        this.game_object_layer.addChild(obj.get_sprite());
    }

    add_background_object(obj) {
        this.background_objects.push(obj);
        this.background_layer.addChild(obj.get_sprite());
    }

    add_overlay_object(obj) {
        this.overlay_objects.push(obj);
        this.foreground_layer.addChild(obj);
    }
}

class LevelState extends State {

    game_over: boolean;
    keyboard: Keyboard;
    current_level: number;
    TOT_LEVELS: number;
    player: Player;
    heart: HeartEnemy;
    display_arrow: DisplayArrow;

    level_events: CreateEvent[];

    restart_time:number;

    HEART_TIME;number;
    heart_activated:boolean;
    hide_heart_time:number;

    heart_info_text:PIXI.Text;

    info_text:PIXI.Text;
    info_text_display_duration:number;
    info_text_hide_timing:number;

    is_heart_time_passed(game_time:GameTime) { return game_time.get_time_since_start() > this.HEART_TIME; }

    wave_text: PIXI.Text;
    wave_text_timer: number;
    wave_1_displayed: boolean;
    wave_2_displayed: boolean;
    wave_3_displayed: boolean;

    WAVE_TEXT_DISPLAY_TIME: number;
    WAVE_1_TIME: number;
    WAVE_2_TIME: number;
    WAVE_3_TIME: number;

    PARTICLE_COUNT: number;
    particles: BloodParticle[];
    particle_trails: BloodParticleTrail[];

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent) {
        super(state_manager, pixi_stage, gc);

    }

    initialize() {
        super.initialize();

        this.game_over = false;
        this.keyboard = new Keyboard();

        var start_x = 50;
        var start_y = 200;

        this.HEART_TIME = 112;
        this.heart_activated = false;

        this.level_events = get_level_events(this.current_level, this.gc);

        this.player = new Player(start_x, start_y, this.gc, this.HEART_TIME);
        this.add_game_object(this.player);

        this.display_arrow = new DisplayArrow(this.player.get_x(), this.player.get_y(), this.gc);
        this.add_game_object(this.display_arrow);

        this.info_text_display_duration = 3;
        this.info_text_hide_timing = -1;

        this.info_text = new PIXI.Text("Space to move up/down\n\nControl to shoot when charge is ready",
            {font:"30px Courier", fill:"white"});
        this.info_text.anchor.x = 0.5;
        this.info_text.anchor.y = 0.5;
        this.info_text.position.x = game_content.level_width / 2;
        this.info_text.position.y = game_content.level_height / 2 - 20;
        this.info_text.visible = true;
        this.add_overlay_object(this.info_text);

        this._init_wave_text();
        this._init_particles();
    }

    deinitialize() {
        super.deinitialize();
        this.player = undefined;
    }

    update(game_time) {
        super.update(game_time);

        // Info text
        this.infoTextLogic(game_time);

        // Particles
        this._update_particles(game_time);

        // Collisions
        performCollisions(this.game_objects);

        // Arrows
        this.display_arrow.update_arrow(this.player);
        this._update_level_events(this.gc, game_time);

        // Shooting
        if (this.keyboard.isKeyPressed("control") && this.player.is_shot_ready(game_time)) {
            this._update_shooting(this.gc);
            this.player.shooting_is_performed(game_time);
            FIRE_SOUND.play();
        }

        // Update loop
        for (var i = 0; i < this.game_objects.length; i++) {
            var obj = this.game_objects[i];
            obj.update(game_time, this.obstacle_objects, this.game_objects, this.player);
        }

        for (var i = 0; i < this.background_objects.length; i++) {
            var bg_obj = this.background_objects[i];
            bg_obj.update(game_time, this.obstacle_objects, this.game_objects, this.player);
        }

        this._remove_the_dead();

        // Heart logic
        if (this.is_heart_time_passed(game_time) && !this.heart_activated) {
            this._initiate_heart(game_time, game_content);
        }

        // Game over logic
        if ((this.player.get_is_clogged() || this.player.is_dead()) && !this.game_over) {
            this._game_over_logic(game_time);
        }

        if (this.game_over) {
            if (this.keyboard.isKeyPressed('space') && game_time.get_time() > this.restart_time) {
                this.deinitialize();
                this.initialize();
            }
        }
    }

    infoTextLogic(game_time:GameTime) {
        if (this.info_text_hide_timing === -1) {
            this.info_text_hide_timing = game_time.get_time_since_start() + this.info_text_display_duration;
        }

        if (game_time.get_time_since_start() > this.info_text_hide_timing) {
            this.info_text.visible = false;
        }

        if (game_time.get_time_since_start() > this.hide_heart_time) {
            this.heart_info_text.visible = false;
        }

        this._update_wave_text(game_time);
    }

    _initiate_heart(game_time:GameTime, gc:GameContent) {
        this.heart_activated = true;
        this.heart = new HeartEnemy(this.gc);
        this.add_game_object(this.heart);

        var INFO_TIME = 5;

        this.info_text = new PIXI.Text("You have reached the Heart\n\nAvoid the white blood cells\nand CLOG",
            {font:"30px Courier", fill:"white"});
        this.info_text.anchor.x = 0.5;
        this.info_text.anchor.y = 0.5;
        this.info_text.position.x = 300;
        this.info_text.position.y = game_content.level_height / 2 - 20;
        this.info_text.visible = true;
        this.add_overlay_object(this.info_text);

        this.info_text_hide_timing = game_time.get_time_since_start() + INFO_TIME;

        //this.info_text.text = "You have reached the heart!\nAvoid the white blood cells\nCLOG THE HEART!";
        //this.info_text.position.x = 400;
        //this.info_text.visible = true;
        //this.hide_heart_time = game_time.get_time_since_start() + INFO_TIME;

        var white_bloodcell_density = 0.7;
        var red_bloodcell_density = 1.3;
        var duration = 1200;
        var white_cell_nbr = 3;
        var red_cell_nbr = 2;
        var white_bloodcells = get_events_over_duration(gc, game_time.get_time_since_start(), duration,
                                                        white_bloodcell_density, white_cell_nbr);
        var red_bloodcells = get_events_over_duration(gc, game_time.get_time_since_start(), duration,
                                                        red_bloodcell_density, red_cell_nbr);

        for (var i = 0; i < white_bloodcells.length; i++) {
            this.level_events.push(white_bloodcells[i]);
        }

        for (var i = 0; i < red_bloodcells.length; i++) {
            this.level_events.push(red_bloodcells[i]);
        }
    }

    _update_shooting(gc:GameContent) {

        var bullets = 20;

        for (var i = 0; i < bullets; i++) {

            var curr_player_height = this.player.height();
            var ydiff = Math.random() * curr_player_height - curr_player_height / 2;

            var bullet = new Bullet(this.player.get_x() + this.player.get_sprite().width / 2,
                this.player.get_y() + this.player.get_sprite().height / 2 + ydiff, gc);
            this.add_game_object(bullet);

        }
    }

    _init_particles() {
        this.PARTICLE_COUNT = 10;
        this.particles = [];
        this.particle_trails = [];

        for (var i = 0; i < this.PARTICLE_COUNT; i++) {
            var particle = new BloodParticle(1000, 0, this.gc);

            particle.set_x(Math.random() * this.gc.level_width);
            particle.set_y(Math.random() * (this.gc.playable_area_height - particle.SPAWN_Y_OFFSET * 2)
                                  + this.gc.playable_area_offset + particle.SPAWN_Y_OFFSET);

            this.particles.push(particle);
            this.add_background_object(particle);
        }
    }

    _update_particles(game_time:GameTime) {
        var time = game_time.get_time_since_start();

        for (var i = 0; i < this.PARTICLE_COUNT; i++) {
            var particle = this.particles[i];

            if (particle.get_x() < 0) {
                particle.set_x(this.gc.level_width);
                particle.set_y(Math.random() * (this.gc.playable_area_height - particle.SPAWN_Y_OFFSET * 2)
                                      + this.gc.playable_area_offset + particle.SPAWN_Y_OFFSET);
            }

            // Create trails
            if (time > particle.trail_spawn_time) {
                var particle_trail = new BloodParticleTrail(particle.get_x(), particle.get_y(), this.gc);
                particle_trail.death_time = time + particle_trail.LIFE_TIME;
                this.particle_trails.push(particle_trail);
                this.add_background_object(particle_trail);

                particle.trail_spawn_time = time + particle.TRAIL_SPAWN_DELAY;
            }
        }

        for (var i = 0; i < this.particle_trails.length; i++) {
            var particle_trail = this.particle_trails[i];

            particle_trail.sprite.alpha -= particle_trail.ALPHA_DECAY_RATE;

            if (time > particle_trail.death_time) {
                particle_trail.set_health(0);
            }
        }
    }

    _init_wave_text() {
        this.WAVE_1_TIME = 0;
        this.WAVE_2_TIME = 36;
        this.WAVE_3_TIME = 71;
        this.WAVE_TEXT_DISPLAY_TIME = 3;

        this.wave_text = new PIXI.Text("", {font:"40px Courier", fill:"white"});
        this.wave_text.anchor.x = 0.5;
        this.wave_text.position.x = 400;
        this.wave_text.position.y = 270;
        this.wave_text.visible = false;
        this.add_overlay_object(this.wave_text);
    }

    _update_wave_text(game_time:GameTime) {
        if (this.wave_text.visible) {
            if (game_time.get_time_since_start() > this.wave_text_timer) {
                this._hide_wave_text();
            }
        }
        else {
            if (game_time.get_time_since_start() > this.WAVE_3_TIME && !this.wave_3_displayed) {
                this.wave_3_displayed = true;
                this.wave_text.text = "Wave 3";
                this._show_wave_text(game_time);
            }
            else if (game_time.get_time_since_start() > this.WAVE_2_TIME && !this.wave_2_displayed) {
                this.wave_2_displayed = true;
                this.wave_text.text = "Wave 2";
                this._show_wave_text(game_time);
            }
            else if (game_time.get_time_since_start() > this.WAVE_1_TIME && !this.wave_1_displayed) {
                this.wave_1_displayed = true;
                this.wave_text.text = "Wave 1";
                this._show_wave_text(game_time);
            }
        }
    }

    _show_wave_text(game_time:GameTime) {
        this.wave_text_timer = game_time.get_time_since_start() + this.WAVE_TEXT_DISPLAY_TIME;
        this.wave_text.visible = true;
    }

    _hide_wave_text() {
        this.wave_text.visible = false;
    }

    _update_level_events(gc:GameContent, current_time:GameTime) {

        for (var i = 0; i < this.level_events.length; i++) {
            var lv_event = this.level_events[i];
            if (lv_event.is_ready(current_time)) {
                var creature = lv_event.retrieve_creature(gc);
                this.add_game_object(creature);
            }
        }

        var to_remove = [];
        for (var i = 0; i < this.level_events.length; i++) {
            var lv_event = this.level_events[i];
            if (lv_event.is_used) {
                to_remove.push(lv_event);
            }
        }
        for (var i = 0; i < to_remove.length; i++) {
            var lv_event_to_remove = to_remove[i];
            var target_index = this.level_events.indexOf(lv_event_to_remove);
            this.level_events.splice(target_index, 1);
        }
    }

    _game_over_logic(game_time:GameTime) {

        this.game_over_text = new PIXI.Text("", {font:"30px Courier", fill:"white"});
        this.display_arrow.sprite.visible = false;

        if (this.player.is_dead()) {
            this.game_over_text = new PIXI.Text("You were exterminated by the Heart!\nSpace to restart", {font:"30px Courier", fill:"white"});
            this.game_over_text.position.x = 40;
            this.game_over_text.position.y = 50;
        }
        else {
            if (this.heart_activated) {
                this.game_over_text = new PIXI.Text("You successfully clogged the Heart!\nYour host is dead now, and you are free\n\nMade by LinkPact Games for Ludum Dare 34\n\nwww.LinkPact.com\n\nThank you for playing!!", {font:"30px Courier", fill:"white"});
                this.game_over_text.position.x = 40;
                this.game_over_text.position.y = 50;
                //this.state_manager.change_stage("win");
            }
            else {
                this.game_over_text = new PIXI.Text("Oh no, you clogged the artery\n\nYou won't reach the heart now!\nSpace to restart", {font:"30px Courier", fill:"white"});
                this.game_over_text.position.x = 40;
                this.game_over_text.position.y = 50;
            }
        }

        this.pixi_stage.addChild(this.game_over_text);
        this.game_over = true;

        var RESTART_DELAY = 2;
        this.restart_time = game_time.current_time + RESTART_DELAY;
    }

    _remove_the_dead() {

        var marked_by_death_array = [];
        for (var i = 0; i < this.game_objects.length; i++) {
            if (this.game_objects[i].is_dead()) {
                marked_by_death_array.push(this.game_objects[i]);
            }
        }

        for (var i = 0; i < marked_by_death_array.length; i++) {

            var target_obj = marked_by_death_array[i];
            this.game_object_layer.removeChild(target_obj.get_sprite());
            var obj_index = this.game_objects.indexOf(target_obj);
            this.game_objects.splice(obj_index, 1);
        }

        var dead_bg_objects = [];
        for (var i = 0; i < this.background_objects.length; i++) {
            if (this.background_objects[i].is_dead()) {
                dead_bg_objects.push(this.background_objects[i]);
            }
        }

        for (var i = 0; i < dead_bg_objects.length; i++) {
            var bg_obj = dead_bg_objects[i];
            this.background_layer.removeChild(bg_obj.get_sprite());
            var obj_index = this.background_objects.indexOf(bg_obj);
            this.background_objects.splice(obj_index, 1);
            obj_index = this.particle_trails.indexOf(bg_obj);
            this.particle_trails.splice(obj_index, 1);
        }
    }
}

class LevelInstance extends LevelState {

    current_level: number;
    TOT_LEVELS: number;

    constructor(state_manager:StateManager, stage:PIXI.Container, gc:GameContent) {
        super(state_manager, stage, gc);
        this.current_level = 1;
        this.TOT_LEVELS = 3;
    }
}

class MenuState extends State {

    keyboard:Keyboard;

    start_time:number;
    delay:number;

    text_stack:string[];
    event_index:number;
    next_event_time:number;

    upper_text:PIXI.Text;
    mid_text:PIXI.Text;
    lower_text:PIXI.Text;

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent) {
        super(state_manager, pixi_stage, gc)
    }

    initialize() {
        super.initialize();

        this.keyboard = new Keyboard();

        this.state_manager.time_reset();

        this.upper_text = new PIXI.Text("Don't Clog the Artery!", {font:"50px Courier", fill:"white"});
        this.upper_text.anchor.x = 0.5;
        this.upper_text.position.x = 400;
        this.upper_text.position.y = 50;
        this.add_overlay_object(this.upper_text);

        this.mid_text = new PIXI.Text("Press space (or Control to skip intro)", {font:"20px Courier", fill:"white"});
        this.mid_text.anchor.x = 0.5;
        this.mid_text.position.x = 400;
        this.mid_text.position.y = 270;
        this.add_overlay_object(this.mid_text);

        this.lower_text = new PIXI.Text("Controls: Space/Control", {font:"40px Courier", fill:"white"});
        this.lower_text.anchor.x = 0.5;
        this.lower_text.position.x = 400;
        this.lower_text.position.y = 170;
        this.add_overlay_object(this.lower_text);

        this.start_time = 0;
        this.delay = 0.3;

        this.event_index = -1;
        this.next_event_time = 0;

        this.text_stack = [];
        this.text_stack.push("You are a lone cell, and you have a mission");
        this.text_stack.push("You want to get rid of your host, who is keeping you imprisoned");
        this.text_stack.push("You will use the only tool you know");
        this.text_stack.push("You will CLOG");
        this.text_stack.push("Remember, clogging an artery won't make the difference");
        this.text_stack.push("Don't clog the artery, clog the HEART");
    }

    update(game_time) {
        super.update(game_time);

        if (this.keyboard.isKeyPressed("control")) {
            this.event_index = this.text_stack.length;
        }

        if (this.keyboard.isKeyPressed("space") && game_time.get_time_since_start() > this.next_event_time) {
            this.event_index += 1;
            this.next_event_time = game_time.get_time_since_start() + this.delay;
        }

        if (this.event_index >= this.text_stack.length) {
            this.state_manager.change_stage("level");
        }

        if (this.event_index < this.text_stack.length && this.event_index >= 0) {
            this.mid_text.text = this.text_stack[this.event_index];
        }
    }
}

class WinState extends State {

    keyboard: Keyboard;
    exit_allowed_time:number;
    delay_time:number;

    constructor(state_manager:StateManager, stage:PIXI.Container, gc:GameContent) {
        super(state_manager, stage, gc);
        this.keyboard = new Keyboard();
        this.exit_allowed_time = -1;
    }

    initialize() {
        super.initialize();

        var text = new PIXI.Text("You successfully clogged the Heart!\nYour host is dead now, and you are free\n\nMade by LinkPact Games for Ludum Dare 34\n\nwww.LinkPact.com\n\nThanks for playing!!",
            {font:"30px Courier", fill:"white"});
        text.position.y = 70;
        this.add_overlay_object(text);
    }

    update(game_time) {
        super.update(game_time);

        if (this.exit_allowed_time === -1) {
            this.exit_allowed_time = game_time.get_time_since_start() + this.delay_time;
        }

        if (this.keyboard.isKeyPressed('space') && game_time.get_time_since_start() > this.exit_allowed_time) {
            this.state_manager.change_stage('level');
        }
    }
}