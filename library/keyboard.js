var Keyboard = (function () {
    function Keyboard() {
        this.keys = {};
        this._setup_default_keys();
    }
    Keyboard.prototype._setup_default_keys = function () {
        this.keys['left'] = new CustomKey(37);
        this.keys['up'] = new CustomKey(38);
        this.keys['right'] = new CustomKey(39);
        this.keys['down'] = new CustomKey(40);
        this.keys['space'] = new CustomKey(32);
        this.keys['control'] = new CustomKey(17);
    };
    Keyboard.prototype.addKey = function (keycode) {
        this.keys[keycode] = new CustomKey(keycode);
    };
    Keyboard.prototype.isKeyPressed = function (keycode) {
        return this.keys[keycode].isDown;
    };
    return Keyboard;
})();
var CustomKey = (function () {
    function CustomKey(keycode) {
        var that = this;
        this.code = keycode;
        this.isDown = false;
        this.isUp = true;
        this.press = undefined;
        this.release = undefined;
        this.downHandler = function (event) {
            if (event.keyCode === that.code) {
                if (that.isUp) {
                    that.isDown = true;
                    that.isUp = false;
                }
            }
            event.preventDefault();
        };
        this.upHandler = function (event) {
            if (event.keyCode === that.code) {
                that.isDown = false;
                that.isUp = true;
            }
            event.preventDefault();
        };
        //Attach event listeners
        window.addEventListener("keydown", this.downHandler.bind(this.key), false);
        window.addEventListener("keyup", this.upHandler.bind(this.key), false);
    }
    return CustomKey;
})();
