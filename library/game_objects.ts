/// <reference path="../external_libraries/pixi.js.d.ts" />
/// <reference path="movement.ts" />
/// <reference path="animation.ts" />
/// <reference path="game_utilities.ts" />
/// <reference path="enemies.ts" />

// ------ Classes ------ //

class GameObject {

    gc: GameContent;

    source_rect: PIXI.Rectangle;
    sprite: PIXI.Sprite;
    animations: Object;
    current_anim_name: string;
    current_anim: Animation;
    movement: Movement;
    is_lightsource: boolean;
    logging: boolean;
    static_only: boolean;
    alpha: number;

    max_health: number;
    current_health: number;

    player_dir: any;
    label: string;

    scaling:number;

    next_direction:boolean;
    set_next_direction(val:boolean) { this.next_direction = val; }
    get_next_direction() { return this.next_direction; }

    constructor(texture_sheet:PIXI.Texture, static_rect:PIXI.Rectangle, xpos:number, ypos:number, gc:GameContent) {

        this.source_rect = static_rect;

        var base_text = new PIXI.Texture(texture_sheet.baseTexture, new PIXI.Rectangle(static_rect.x, static_rect.y, static_rect.width, static_rect.height));
        this.sprite = new PIXI.Sprite(base_text);
        this.animations = {};
        this.current_anim_name = 'static';
        this.animations[this.current_anim_name] = new Animation(texture_sheet, static_rect);
        this.current_anim = this.animations[this.current_anim_name];

        this.set_x(xpos);
        this.set_y(ypos);

        this.alpha = 1;
        this.movement = undefined;

        this.logging = false;
        this.label = 'gameobject';

        this.scaling = 1;

        this.gc = gc;

        this.next_direction = false;
    }

    is_dead() { return this.current_health <= 0; }

    get_sprite() { return this.sprite; }
    change_animation(name:string) {

        if (name === this.current_anim_name) {
            return;
        }
        this.current_anim_name = name;
        this.current_anim = this.animations[this.current_anim_name];
    }

    get_x() { return this.sprite.position.x; }
    set_x(value) { this.sprite.position.x = value; }
    increment_x(increment) { this.sprite.position.x += increment; }

    get_y() { return this.sprite.position.y; }
    set_y(value) { this.sprite.position.y = value; }
    increment_y(increment) { this.sprite.position.y += increment; }

    set_health(val:number) { this.current_health = val; }
    get_health() { return this.current_health; }

    width() { return this.source_rect.width * this.scaling; }
    height() { return this.source_rect.height * this.scaling; }

    get_alpha() { return this.sprite.alpha; }
    set_alpha(value) { this.sprite.alpha = value; }

    set_scaling(val:number) {
        this.scaling = val;
        this.sprite.scale.x = this.scaling;
        this.sprite.scale.y = this.scaling;
    }

    update(game_time, obstacles, game_objects, player) {

        if (this.movement !== undefined) {
            this.movement.update(game_time, obstacles, player);
        }

        if (!this.static_only) {
            this.current_anim.update(game_time);
            if (this.current_anim.has_new_frame()) {
                this.sprite.texture = this.current_anim.get_current_frame();
            }
        }
    }

    get_closest_object(objects:GameObject[]) {

        var closest_obj = null;
        var closest_dist = 1000;
        for (var i = 0;  i < objects.length; i++) {
            var obj = objects[i];
            var distance = this.get_dist_to_object(obj);
            if (distance < closest_dist) {
                closest_dist = distance;
                closest_obj = obj;
            }
        }
        return closest_obj;
    }

    get_dist_to_object(other_obj:GameObject) {
        return Math.sqrt(Math.pow(this.get_x() - other_obj.get_x(), 2) + Math.pow(this.get_y() - other_obj.get_y(), 2));
    }

    collide(other_obj:GameObject) {

    }
}

class PlatformGameObject extends GameObject {

    constructor(spritesheet, rect, x, y, gc) {
        super(spritesheet, rect, x, y, gc);

        this.label = 'platformgameobject';
    }

    get_yspeed() {
        return this.movement.yspeed;
    }

    set_yspeed(value) {
        this.movement.yspeed = value;
    }

    get_is_jumping() {
        return this.movement.is_jumping;
    }

    set_is_jumping(value) {
        this.movement.is_jumping = value;
    }

    get_grounded() {
        return this.movement.grounded;
    }

    set_grounded(value) {
        this.movement.grounded = value;
    }
}

class Player extends PlatformGameObject {

    level_win:boolean;

    clog_amount:number;
    game_content:GameContent;

    next_shot_time:number;
    BULLET_DELAY:number;
    CLOG_REDUCTION:number;

    is_clogged:boolean;
    set_is_clogged() { this.is_clogged = true; }
    get_is_clogged() { return this.is_clogged; }

    heart_appear_time:number;

    INITIAL_SCALE:number;

    //get_is_moving() { return this.movement; }

    constructor(x:number, y:number, gc:GameContent, heart_time:number) {
        super(gc.spritesheet, new PIXI.Rectangle(159, 1, 200, 200), x, y, gc);
        this.movement = new PlayerHorizontalScroll(this, gc);
        this.label = 'player';

        this.game_content = gc;

        this.BULLET_DELAY = 5;
        this.next_shot_time = -1;
        this.CLOG_REDUCTION = 0.5;
        this.INITIAL_SCALE = 0.16;

        this.animations['animated_static'] = new Animation(gc.spritesheet, new PIXI.Rectangle(159, 1, 200, 200), 3, 1, 5);
        this.change_animation('animated_static');

        this.max_health = 80;
        this.current_health = this.max_health;

        this.is_clogged = false;
        this.level_win = false;
        this.clog_amount = 0;

        this.heart_appear_time = heart_time;
    }

    update(game_time, obstacles, game_objects) {
        super.update(game_time, obstacles, game_objects, this);

        if (this.clog_amount < -0.75) {
            this.current_health = 0;
        }

        if (this.is_clogged || this.is_dead()) {
            this.get_sprite().tint = 0xFFFFFF;
            return;
        }

        if (this.is_shot_ready(game_time)) {
            this.get_sprite().tint = 0x00AA00;
        }
        else {
            this.get_sprite().tint = 0xFFFFFF;
        }

        this.set_scaling(this.calculate_scale(this.clog_amount));
    }

    is_shot_ready(game_time:GameTime) {
        if (this.next_shot_time === -1) {
            this.next_shot_time = game_time.get_time() + this.BULLET_DELAY;
            return false;
        }
        else {
            return game_time.get_time() >= this.next_shot_time;
        }
    }

    shooting_is_performed(game_time:GameTime) {
        this.next_shot_time = game_time.get_time() + this.BULLET_DELAY;

        if (game_time.get_time_since_start() < this.heart_appear_time) {
            this.clog_amount = Math.max(this.clog_amount - this.clog_amount * this.CLOG_REDUCTION, 0);
        }
    }

    collide(other_obj:GameObject) {
        if (other_obj instanceof Enemy) {
            this.clog_amount += other_obj.get_damage();
            SOFT_HIT_SOUND2.play();
        }
      }

    calculate_scale(clog_amount:number) {
        return (clog_amount + 1) * this.INITIAL_SCALE;
    }
}

class Bullet extends GameObject {
    x_change_speed: number;
    get_x_change_speed() { return this.x_change_speed; }

    constructor(x, y, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(1, 369, 10, 10), x, y, gc);

        var max_speed = 8;
        var min_speed = 2;
        var speed = Math.random() * (max_speed - min_speed) + min_speed;

        this.movement = new SimpleHorizontal(this, gc, -speed);

        this.x_change_speed = 1.2;
    }

    update(gameTime:GameTime, obstacles:GameObject[], game_objects:GameObject[], player:Player) {
        super.update(gameTime, obstacles, game_objects, player);

        var rough_screen_width = 1000;

        if (this.get_x() > rough_screen_width) {
            this.set_health(0);
        }
    }

    collide(other_obj:GameObject) {
        if (other_obj instanceof Enemy) {
            this.set_health(0);
        }
    }
}

class BloodParticle extends GameObject {

    TRAIL_SPAWN_DELAY: number;
    SPAWN_Y_OFFSET: number;
    trail_spawn_time: number;

    constructor(x:number, y:number, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(1, 353, 3, 3), x, y, gc);

        this.TRAIL_SPAWN_DELAY = 0.01;
        this.SPAWN_Y_OFFSET = 50;
        this.trail_spawn_time = 0;

        var max_speed = 6;
        var min_speed = 3;
        var speed = Math.random() * (max_speed - min_speed) + min_speed;

        this.movement = new SimpleHorizontal(this, gc, speed);
    }
}

class BloodParticleTrail extends GameObject {

    LIFE_TIME: number;
    ALPHA_DECAY_RATE: number;

    death_time:number;
    alpha: number;

    constructor(x:number, y:number, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(1, 361, 18, 3), x, y, gc);

        this.LIFE_TIME = 0.6;
        this.ALPHA_DECAY_RATE = 0.06;
    }
}

class DisplayArrow extends GameObject {

    current_direction:boolean;
    Y_OFFSET:number;

    constructor(x, y, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(1, 295, 20, 17), x, y, gc);

        this.current_direction = false;
        this.Y_OFFSET = 25;

        this.animations['up'] = new Animation(gc.spritesheet, new PIXI.Rectangle(1, 295, 20, 17));
        this.animations['down'] = new Animation(gc.spritesheet, new PIXI.Rectangle(26, 295, 20, 17));
        this.animations['up_inactive'] = new Animation(gc.spritesheet, new PIXI.Rectangle(51, 295, 20, 17));
        this.animations['down_inactive'] = new Animation(gc.spritesheet, new PIXI.Rectangle(76, 295, 20, 17));
    }

    update_arrow(player:Player) {

        var is_moving = player.movement.get_is_moving();
        var dir = player.get_next_direction();
        this.set_x(player.get_x() + player.width() / 2 - 8);

        if (dir) {

            if (is_moving) {
                this.change_animation('up');
            }
            else {
                this.change_animation('up_inactive');
            }

            this.set_y(player.get_y() - this.Y_OFFSET);
        }
        else {

            if (is_moving) {
                this.change_animation('down');
            }
            else {
                this.change_animation('down_inactive');
            }

            this.set_y(player.get_y() + player.height() + this.Y_OFFSET - this.sprite.height);
        }
    }
}



