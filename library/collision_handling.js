/// <reference path="movement.ts" />
/// <reference path="game_objects.ts" />
function performCollisions(game_objects) {
    for (var i = 0; i < game_objects.length; i++) {
        var obj1 = game_objects[i];
        for (var j = i + 1; j < game_objects.length; j++) {
            var obj2 = game_objects[j];
            if (intersectObjects(obj1, obj2)) {
                obj1.collide(obj2);
                obj2.collide(obj1);
            }
        }
    }
}
function correctingColCheck(obj1, obj2) {
    var vectorX = (obj1.get_x() + obj1.width() / 2) - (obj2.get_x() + obj2.width() / 2);
    var vectorY = (obj1.get_y() + obj1.height() / 2) - (obj2.get_y() + obj2.height() / 2);
    var halfWidths = (obj1.width() / 2) + (obj2.width() / 2);
    var halfHeights = (obj1.height() / 2) + (obj2.height() / 2);
    var colDir = Direction.NONE;
    if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {
        var offsetX = halfWidths - Math.abs(vectorX);
        var offsetY = halfHeights - Math.abs(vectorY);
        if (offsetX >= offsetY) {
            if (vectorY > 0) {
                colDir = Direction.UP;
                obj1.increment_y(offsetY);
            }
            else {
                colDir = Direction.DOWN;
                obj1.increment_y(-offsetY);
            }
        }
        else {
            if (vectorX > 0) {
                colDir = Direction.LEFT;
                obj1.increment_x(offsetX);
            }
            else {
                colDir = Direction.RIGHT;
                obj1.increment_x(-offsetX);
            }
        }
    }
    return colDir;
}
function intersectObjects(obj1, obj2) {
    return !(obj2.get_x() > obj1.get_x() + obj1.width() || obj2.get_x() + obj2.width() < obj1.get_x() || obj2.get_y() > obj1.get_y() + obj1.height() || obj2.get_y() + obj2.height() < obj1.get_y());
}
