/// <reference path="game_objects.ts" />

class Enemy extends GameObject {

    damage:number;

    get_damage() { return this.damage; }

    constructor(spritesheet:PIXI.Texture, rect:PIXI.Rectangle, lv_w:number, y:number, gc:GameContent) {
        super(spritesheet, rect, lv_w, y, gc);

        this.damage = 0;
    }

    update(gameTime:GameTime, obstacles:GameObject[], game_objects:GameObject[], player:Player) {
        super.update(gameTime, obstacles, game_objects, player);

        if (this.get_x() < -100) {
            this.set_health(0);
        }
    }

    collide(other_obj:GameObject) {
        if (other_obj instanceof Player) {
            this.set_health(0);
        }
        else if (other_obj instanceof Bullet) {
            this.movement.set_xspeed(this.movement.get_xspeed() + other_obj.get_x_change_speed());
            if (this.movement.get_xspeed() > 0) {
                this.set_health(0);
            }

            SOFT_HIT_SOUND.play();
        }
    }
}

class SimpleEnemy extends Enemy {

    constructor(y:number, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(38, 61, 39, 27), gc.level_width, y, gc);
        this.label = 'simpleenemy';

        //var max_speed = 4;
        var min_speed = 8;
        var variance = 2;
        var speed = Math.random() * variance + min_speed; /*Math.random() + (max_speed - min_speed) + min_speed;*/

        this.damage = 0.3;

        this.movement = new SimpleHorizontal(this, gc, speed);
    }
}

class WhiteBloodCell extends Enemy {
    constructor(y:number, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(38, 316, 39, 28), gc.level_width, y, gc);
        this.label = 'whitebloodcell';

        var min_speed = 13;
        var variance = 1;
        var speed = Math.random() * variance + min_speed;

        this.damage = -0.45;

        this.movement = new SimpleHorizontal(this, gc, speed);
    }
}

class DirectingEnemy extends Enemy {

    constructor(y:number, gc:GameContent) {
        super(gc.spritesheet, new PIXI.Rectangle(1, 38, 64, 18), gc.level_width, y, gc);
        this.label = 'directingenemy';

        //var max_speed = 8;
        var min_speed = 8;
        var variance = 2;
        var speed = Math.random() * variance + min_speed; /*Math.random() + (max_speed - min_speed) + min_speed;*/

        var yspeed = 0.05;

        this.damage = 0.6;

        this.movement = new AimingHorizontal(this, gc, speed, yspeed);
    }
}

class HeartEnemy extends GameObject {

    constructor(gc:GameContent) {
        var xpos = 600;
        var ypos = gc.level_height / 2 - 110;
        super(gc.spritesheet, new PIXI.Rectangle(206, 234, 160, 192), xpos, ypos, gc);
        this.label = 'heartenemy';

        this.animations['animated_static'] = new Animation(gc.spritesheet, new PIXI.Rectangle(206, 234, 160, 192), 3, 0.4, 5);
        this.change_animation('animated_static');
    }


}




