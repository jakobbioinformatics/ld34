/// <reference path="main_logic.ts" />
var GameContent = (function () {
    function GameContent() {
        this.level_width = 800;
        this.level_height = 420;
        this.playable_area_height = 240;
        this.playable_area_offset = 80;
        this.tile_size = 16;
        //this.xtiles = this.level_width / this.tile_size;
        //this.ytiles = this.level_height / this.tile_size;
        this.xtiles = 240;
        this.ytiles = 14;
        this.total_tiles = this.xtiles * this.ytiles;
        this.total_levels = 5;
        this.templatetilesheet = PIXI.Texture.fromImage("template_sprites/template_spritesheet.png");
        this.spritesheet = PIXI.Texture.fromImage("template_sprites/spritesheet.png");
        this.background = PIXI.Texture.fromImage("template_sprites/bg.png");
        //this.tilesheet = PIXI.Texture.fromImage("template_sprites/Tileset.png");
        //this.enemysheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        //this.firesheet = PIXI.Texture.fromImage("template_sprites/Fire.png");
        //this.playersheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        //this.tileset16sheet = PIXI.Texture.fromImage("template_sprites/tileset16.png");
    }
    return GameContent;
})();
var GameTime = (function () {
    function GameTime(last_state_change) {
        this.start_time = last_state_change;
        this.date_obj = new Date();
        this.current_time = this.date_obj.getTime() / 1000;
    }
    GameTime.prototype.update = function () {
        this.previous_time = this.current_time;
        this.current_time = this.date_obj.getTime() / 1000;
    };
    GameTime.prototype.get_time = function () {
        return this.current_time;
    };
    GameTime.prototype.get_elapsed_time = function () {
        return this.current_time - this.previous_time;
    };
    GameTime.prototype.get_time_since_start = function () {
        return this.current_time - this.start_time;
    };
    return GameTime;
})();
