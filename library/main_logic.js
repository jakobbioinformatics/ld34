/// <reference path="game_utilities.ts" />
/// <reference path="game_objects.ts" />
/// <reference path="states.ts" />
/// <reference path="levels.ts" />
var pixi_stage = new PIXI.Container();
var renderer;
var game_content = new GameContent();
//var game_time = new GameTime();
var state_manager = new StateManager(pixi_stage, game_content);
var SOFT_HIT_SOUND = new Howl({
    urls: ['template_audio/soft_hit2.wav'],
    volume: 0.5
});
var SOFT_HIT_SOUND2 = new Howl({
    urls: ['template_audio/soft_hit.wav'],
    volume: 0.5
});
var FIRE_SOUND = new Howl({
    urls: ['template_audio/fire.wav'],
    volume: 0.5
});
function init() {
    var sound = new Howl({
        urls: ['template_audio/clog_the_artery.ogg', 'template_audio/clog_the_artery.mp3'],
        loop: true
    }).play();
    renderer = PIXI.autoDetectRenderer(game_content.level_width, game_content.level_height);
    document.body.appendChild(renderer.view);
    renderer.backgroundColor = 0x444444;
    requestAnimationFrame(update);
}
function update() {
    renderer.render(pixi_stage);
    requestAnimationFrame(update);
    state_manager.update();
}
