/// <reference path="main_logic.ts" />

class GameContent {

    level_width:number;
    level_height:number;
    tile_size:number;
    xtiles:number;
    ytiles:number;
    total_tiles:number;
    total_levels:number;
    playable_area_height:number;
    playable_area_offset:number;

    //background:PIXI.Texture;
    //tilesheet:PIXI.Texture;
    //enemysheet:PIXI.Texture;
    //firesheet:PIXI.Texture;
    //playersheet:PIXI.Texture;
    //tileset16sheet:PIXI.Texture;

    background:PIXI.Texture;
    spritesheet:PIXI.Texture;
    templatetilesheet:PIXI.Texture;

    constructor() {

        this.level_width = 800;
        this.level_height = 420;

        this.playable_area_height = 240;
        this.playable_area_offset = 80;

        this.tile_size = 16;
        //this.xtiles = this.level_width / this.tile_size;
        //this.ytiles = this.level_height / this.tile_size;
        this.xtiles = 240;
        this.ytiles = 14;
        this.total_tiles = this.xtiles * this.ytiles;

        this.total_levels = 5;

        this.templatetilesheet = PIXI.Texture.fromImage("template_sprites/template_spritesheet.png");
        this.spritesheet = PIXI.Texture.fromImage("template_sprites/spritesheet.png");
        this.background = PIXI.Texture.fromImage("template_sprites/bg.png");

        //this.tilesheet = PIXI.Texture.fromImage("template_sprites/Tileset.png");
        //this.enemysheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        //this.firesheet = PIXI.Texture.fromImage("template_sprites/Fire.png");
        //this.playersheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        //this.tileset16sheet = PIXI.Texture.fromImage("template_sprites/tileset16.png");
    }
}

class GameTime {

    date_obj:Date;
    current_time:number;
    previous_time:number;
    start_time:number;

    constructor(last_state_change:number) {
        this.start_time = last_state_change;
        this.date_obj = new Date();
        this.current_time = this.date_obj.getTime() / 1000;
    }

    update() {
        this.previous_time = this.current_time;
        this.current_time = this.date_obj.getTime() / 1000;
    }

    get_time() {
        return this.current_time;
    }

    get_elapsed_time() {
        return this.current_time - this.previous_time;
    }

    get_time_since_start() {
        return this.current_time - this.start_time;
    }
}

